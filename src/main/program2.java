package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class program2 {

	public static Connection conn;
	private static Scanner keyboard1 = null;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			Scanner connStrSource = new Scanner(new File("src/connString.txt"));
			String[] connString = connStrSource.nextLine().trim().split("\\s+");
			connStrSource.close();
			
			conn = DriverManager.getConnection(connString[0].trim(), connString[1].trim(), connString[2].trim());
			System.out.println("Database connection established");
			
			keyboard1 = new Scanner(System.in);
			System.out.print("Enter a ticker symbol: ");
			String ticker = keyboard1.nextLine().trim();
			while (!ticker.matches("")) {
				processTicker(ticker);
				System.out.print("\nEnter a ticker symbol: ");
				ticker = keyboard1.nextLine().trim();
			}
			conn.close();
			System.out.println("Database connection closed");
			keyboard1.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private static String quote(String str) {
		return "'" + str + "'";
	}
	private static void processTicker(String ticker) {
		 class TickerData {
				public String date;
				public double open;
				public double close;
			}
		 String companyName = "";
		 ArrayList<TickerData> dataArray = new ArrayList<TickerData>();
		 try {
				Statement stat = conn.createStatement();
				ResultSet rs = stat.executeQuery("select * from Company where Ticker = " + quote(ticker));
				if (!rs.next())
					System.out.println("Ticker " + ticker + " not found in database");
				else {
					System.out.print("Company data for " + ticker + ": ");
					System.out.print("   \n" + rs.getString(2).trim());
					System.out.println();
				}
				rs = stat.executeQuery("select * from PriceVolume where Ticker = " + quote(ticker) + " order by TransDate DESC");
				if (!rs.next()) 
					System.out.println("\nNo data for ticker " + ticker);
				else {
//					System.out.println("\nData for ticker " + ticker + ":");
					int n = rs.getMetaData().getColumnCount();
//					System.out.print("Count ");
//					for (int i = 1; i <= n; i++)
//						System.out.print(String.format("%10s ", rs.getMetaData().getColumnName(i)));
					System.out.println();
					boolean done = false;
					int cnt = 0;
					double open = 1;
					double divisor = 1;
					while (!done) {
						TickerData data = new TickerData();
						data.date = rs.getString(2).trim();
						data.open = Double.parseDouble(rs.getString(3).trim());
						data.close = Double.parseDouble(rs.getString(6));
						
						double ratio = data.close / open;
						
						
						if(Math.abs(ratio - 2.0) < 0.13){
							System.out.println("2:1 split on " + data.date + "; " + data.close + " --> " + open);
							divisor *= 2.0;
						}else if(Math.abs(ratio - 3.0) < 0.13){
							System.out.println("3:1 split on " + data.date + "; " + data.close + " --> " + open);
							divisor *= 3.0;
						}else if(Math.abs(ratio - 1.5) < 0.13){
							System.out.println("3:2 split on " + data.date + "; " + data.close + " --> " + open);
							divisor *= 1.5;
						}
						
						open = data.open;
						data.open =  data.open / divisor;
						data.close = data.close / divisor;						
						
						dataArray.add(0, data);
						cnt = cnt + 1;
//						System.out.print(String.format("%5d ", cnt));
//						for (int i = 1; i <= n; i++)
//							System.out.print(String.format("%10s ", rs.getString(i).trim()));
//						System.out.println();
						if (!rs.next())
							done = true;
					}
//					System.out.println("\nData tuples stored: " + dataArray.size());
					System.out.println("Executing investment strategy");
//					for (int i = 0; i < dataArray.size(); i++) 
//						System.out.println(String.format("     %s, %7.2f, %7.2f", dataArray.get(i).date, dataArray.get(i).open, dataArray.get(i).close));
					double sum = 0;
					double avg = 0;
					int stocks = 0;
					double total = 0;
					int transactions = 0;
					int buys = 0;
					int sells = 0;
					for(int i = 0; i < cnt-1; i++){
						if(i < 50){
							sum = sum + dataArray.get(i).close;
						}
						else{
							avg = sum / 50;
//							if(i % 10 == 0)
//							ticker = keyboard1.nextLine().trim();
//						    System.out.println(dataArray.get(i).date + "     " + dataArray.get(i).open + "     " + dataArray.get(i).close);
//							System.out.println("avg  =   " + avg);
							if(dataArray.get(i).close < avg && dataArray.get(i).open - dataArray.get(i).close > dataArray.get(i).open * 0.03){
								stocks += 100;
								total -= 100 * dataArray.get(i+1).open; 
								total -= 8;
								transactions++;
								buys++;
								
//								System.out.println(dataArray.get(i).date + "   Buying 100");
							}
							else if(stocks >= 100 & dataArray.get(i).open > avg && dataArray.get(i).open - dataArray.get(i-1).close> dataArray.get(i-1).close * 0.01){
								stocks -= 100;
								total += 50 * (dataArray.get(i).open + dataArray.get(i).close);
								total -= 8;
								transactions++;
								sells++;
//								System.out.println(dataArray.get(i).date + "   Selling 100");
							}
							sum = sum + dataArray.get(i).close - dataArray.get(i-50).close;
						}
					}
					System.out.println("Transactions executed:   " + transactions);
					double net = dataArray.get(cnt-1).open * stocks + total;
					System.out.println(total);
					System.out.println(stocks);
					System.out.println("Net Gain:   " + net);
				}
		 }
		 catch (SQLException ex) {
			System.out.println("SQL exception in processTicker");
		}
	} 

		
}
